const fs = require('fs');
const mdncompat = require('mdn-browser-compat-data');

const getAllProps = () => {
  let finalJSON = {};
  for (let type in mdncompat.javascript) {
    for (let key in mdncompat.javascript[type]) {
      for (let subkey in mdncompat.javascript[type][key]) {
        if (subkey === '__compat') {
          finalJSON[`${type}.${key}`] = mdncompat.javascript[type][key][subkey];
        } else {
          for (let innerKey in mdncompat.javascript[type][key][subkey]) {
            if (innerKey === '__compat') {
              finalJSON[`${type}.${key}.${subkey}`] = mdncompat.javascript[type][key][subkey][innerKey];
            }
          }
        }
      }
    }
  }

  try {
    fs.writeFileSync('data.json', JSON.stringify(finalJSON));
    return finalJSON;
  } catch (err) {
    console.log('error writing ', err);
    throw err;
  }
}

module.exports = { getAllProps };