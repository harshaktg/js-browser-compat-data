# 1.0.5 - 18-06-2020

Minor bug fixes

# 1.0.4 - 18-06-2020

Added support when package is used in client

# 1.0.3 - 17-06-2020

Updated documentation

# 1.0.2 - 17-06-2020

Updated documentation

# 1.0.1 - 14-06-2020

Updated documentation

# 1.0.0 - 14-06-2020

✨Initial release
