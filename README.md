# js-browser-compat-data

Checks JS browsers compatibilities based on mdn-browser-compat-data

[![NPM](https://img.shields.io/npm/v/js-browser-compat-data.svg)](https://www.npmjs.com/package/js-browser-compat-data)
[![Downloads](https://img.shields.io/npm/dt/js-browser-compat-data.svg)](https://www.npmjs.com/package/js-browser-compat-data)

## Installation

```console
$ npm i js-browser-compat-data
```

## Usage

```js
const jsCompat = require('js-browser-compat-data');
// or
import * as jsCompat from 'js-browser-compat-data';

jsCompat.find('includes'); // ['builtins.Array.includes','builtins.String.includes','builtins.TypedArray.includes']
jsCompat.getSupport('builtins.Array.includes');
jsCompat.isSupported('builtins.Array.includes', 'ie'); //false
jsCompat.minimumSupportedVersion('builtins.Array.includes', 'chrome'); //47
jsCompat.getStatus('builtins.Array.includes'); // { experimental: false, standard_track: true, deprecated: false }
jsCompat.getMDNLink('builtins.Array.includes'); // https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
jsCompat.allFeatures(); // ['builtins.Array.concat', 'builtins.Array.copyWithin', 'builtins.Array.entries', ...]
```

## API

#### `jsCompat.find(searchKey)`

_get all the methods for the particular search key_

Returns an array of all available JS methods with the given search key. If none of them matches, it returns an empty array.

```js
jsCompat.find('includes');
/*
['builtins.Array.includes', 'builtins.String.includes', 'builtins.TypedArray.includes']
*/
```

#### `jsCompat.getSupport(feature)`

_ask since which browser version the feature is available_

Returns an array of object containing the support for the most used browsers. If the feature name is incorrect, it returns an empty array

```js
jsCompat.getSupport('builtins.Array.includes');
/*
[
  { chrome: { support: true, version: '47' } },
  { chrome_android: { support: true, version: '47' } },
  { edge: { support: true, version: '14' } },
  { firefox: { support: true, version: '43' } },
  { firefox_android: { support: true, version: '43' } },
  { ie: { support: false } },
  { nodejs: { support: true, version: '6.0.0' } },
  { opera: { support: true, version: '34' } },
  { opera_android: { support: true, version: '34' } },
  { safari: { support: true, version: '9' } },
  { safari_ios: { support: true, version: '9' } },
  { samsunginternet_android: { support: true, version: '5.0' } },
  { webview_android: { support: true, version: '47' } }
]
*/
```

#### `jsCompat.isSupported(feature, browser)`

_ask if the feature is supported in a particular browser. The list of browsers are shown above in the previous example_

Returns true if the feature is supported, else it returns false.

```js
jsCompat.isSupported('builtins.Array.includes', 'ie'); // false
jsCompat.isSupported('builtins.Array.includes', 'chrome'); // true
```

#### `jsCompat.minimumSupportedVersion(feature, browser)`

_get the minium version of the browser that supports the feature_

Returns the supported version, if supported, else return false.

```js
jsCompat.minimumSupportedVersion('builtins.Array.includes', 'chrome'); //47
jsCompat.minimumSupportedVersion('builtins.Array.includes', 'ie'); //false
```

#### `jsCompat.getStatus(feature)`

_get the current status of the feature, if it is experimental or deprecated or used as standard_

Returns an object of all these three properties with true/false for each property. Returns and empty object if there is no such feature.

```js
jsCompat.getStatus('builtins.Array.includes');
/*
{ experimental: false, standard_track: true, deprecated: false }
*/
```

#### `jsCompat.getMDNLink(feature)`

_gets the link to the mdn page for the feature_

Returns the URL as the response, else returns empty string if there is no such feature.

```js
jsCompat.getMDNLink('builtins.Array.includes');
/*
https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
*/
```

#### `jsCompat.allFeatures()`

_lists all the features that are available in JS_

Returns an array of all the available features in JS.

```js
jsCompat.allFeatures();
// ['builtins.Array.concat', 'builtins.Array.copyWithin', 'builtins.Array.entries', ...]
```

---

## Issues?

If you find a problem, please [file a bug](https://gitlab.com/harshaktg/js-browser-compat-data/issues/new).

## Note

Though this can be used as a client package, we recommend you to use this as a Node package as it will be in sync with the MDN DB.

## [Changelog](CHANGELOG.md)

## [License](LICENSE)

## <a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/harshavardhan"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:28px !important;"> Buy me a coffee</span></a>
