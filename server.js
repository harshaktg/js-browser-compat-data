var allData = null;
if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  allData = require('./data.json');
} else {
  var fileCopy = require('./copyTask.js');

  allData = fileCopy.getAllProps();
}
var allKeys = Object.keys(allData);

const allFeatures = () => {
  return allKeys;
}

const find = fName => {
  let features = [];
  if (!fName) {
    return features;
  }
  for (let key of allKeys) {
    if (key.includes(fName)) {
      features.push(key);
    }
  }
  return features;
}

const getSupport = fName => {
  let supportedBrowsers = [];
  if (allKeys.includes(fName)) {
    const supportData = allData[fName]['support'];

    for (let browser in supportData) {
      let browserData = {};
      browserData[browser] = {};
      let currBrowser;
      if (Array.isArray(supportData[browser])) {
        currBrowser = supportData[browser][0];
      } else {
        currBrowser = supportData[browser];
      }
      if (currBrowser.hasOwnProperty('version_added') && currBrowser.version_added === false) {
        browserData[browser].support = false;
      } else if (currBrowser.hasOwnProperty('version_removed') && currBrowser.version_removed === false) {
        browserData[browser].support = false;
      } else if (currBrowser.version_removed && currBrowser.version_added && (parseInt(currBrowser.version_removed, 10) > parseInt(currBrowser.version_added, 10))) {
        browserData[browser].support = false;
        browserData[browser].version = currBrowser.version_removed;
      } else {
        browserData[browser].support = true;
        browserData[browser].version = currBrowser.version_added;
      }
      supportedBrowsers.push(browserData);
    }
  }
  return supportedBrowsers;
}

const getStatus = fName => {
  let statusData = {};
  if (allKeys.includes(fName)) {
    statusData = allData[fName]['status'];
  }
  return statusData;
}

const getMDNLink = fName => {
  let url = '';
  if (allKeys.includes(fName)) {
    url = allData[fName]['mdn_url'];
  }
  return url;
}

const isSupported = (fName, reqBrowser) => {
  let allBrowsers = getSupport(fName);
  for (let browser of allBrowsers) {
    if (Object.keys(browser).includes(reqBrowser)) {
      return browser[reqBrowser].support;
    }
  }
  return false;
}

const minimumSupportedVersion = (fName, reqBrowser) => {
  let allBrowsers = getSupport(fName);
  for (let browser of allBrowsers) {
    if (Object.keys(browser).includes(reqBrowser)) {
      if (browser[reqBrowser].support) {
        return browser[reqBrowser].version;
      }
    }
  }
  return false;
}

module.exports = {
  allFeatures,
  find,
  getSupport,
  isSupported,
  minimumSupportedVersion,
  getStatus,
  getMDNLink
};
